//META{"name":"goodnightServers"}*//

var goodnightServers = function() {};

goodnightServers.prototype.name = "Goodnight Servers";
goodnightServers.prototype.namespace = "goodnightServers";

// Variables
goodnightServers.prototype.guildsWrapper = null;
goodnightServers.prototype.tuckedIn = false;

// Settings
goodnightServers.prototype.keyCode = 9;              // Default: 9 (Tab)
goodnightServers.prototype.transitionTime = 0.2;     // Default: 0.2s
goodnightServers.prototype.shouldToggle = false;     // Default: false

// Settings Keys
goodnightServers.prototype.keyCodeKey = "keyCode";
goodnightServers.prototype.transitionTimeKey = "transitionTime";
goodnightServers.prototype.shouldToggleKey = "shouldToggle";

goodnightServers.prototype.start = function()
{
    // Grab the server list element
    this.guildsWrapper = $(".guilds-wrapper")[0];

    if (this.guildsWrapper)
    {
        // Load user settings
        this.loadSettings();

        // Register the key events used to show/hide the server list
        $(document).on(this.namespaceEvent("keydown", this.namespace), (e) => this.keyDown(e));
        $(document).on(this.namespaceEvent("keyup", this.namespace), (e) => this.keyUp(e));

        this.hide();
    }
}

goodnightServers.prototype.stop = function()
{
    if (this.guildsWrapper)
    {
        // Save the settings
        this.saveSettings();

        // Unregister the key events used to show/hide the server list
        $(document).off(this.namespaceEvent("keydown", this.namespace), (e) => this.keyDown(e));
        $(document).off(this.namespaceEvent("keyup", this.namespace), (e) => this.keyUp(e));

        // Show the server list again
        this.show();
    }
}

goodnightServers.prototype.hide = function()
{
    // Sets the transition that "tucks" the server list in when it's hidden
    this.guildsWrapper.style.transition = "width " + this.transitionTime + "s";
    this.guildsWrapper.style.width = "0";

    // Track the hide so we know how to handle other actions
    this.tuckedIn = true;
}

goodnightServers.prototype.show = function()
{
    // Sets the transition that "wakes" the server list in when it's shown
    this.guildsWrapper.style.transition = "width " + this.transitionTime + "s";
    this.guildsWrapper.style.width = "70px";

    // Track the hide so we know how to handle other actions
    this.tuckedIn = false;
}

goodnightServers.prototype.loadSettings = function()
{
    // Load the keyCode setting from storage
    goodnightServers.prototype.keyCode = bdPluginStorage.get(this.namespace, this.keyCodeKey);

    // Load the transitionTime setting from storage
    goodnightServers.prototype.transitionTime = bdPluginStorage.get(this.namespace, this.transitionTimeKey);

    // Load the shouldToggle setting from storage
    goodnightServers.prototype.shouldToggle = bdPluginStorage.get(this.namespace, this.shouldToggleKey);
}

goodnightServers.prototype.saveSettings = function()
{
    // Save the keyCode setting to storage
    bdPluginStorage.set(this.namespace, this.keyCodeKey, this.keyCode);

    // Save the transitionTime setting to storage
    bdPluginStorage.set(this.namespace, this.transitionTimeKey, this.transitionTime);

    // Save the shouldToggle setting to storage
    bdPluginStorage.set(this.namespace, this.shouldToggleKey, this.shouldToggle);
}

goodnightServers.prototype.applySettings = function()
{
    // Get the keyCode value from UI
    goodnightServers.prototype.keyCode = parseInt($("#keyCodeInput").val());

    // Get the transitionTime value from UI
    goodnightServers.prototype.transitionTime = parseFloat($("#transitionTimeInput").val());

    // Get the shouldToggle value from UI
    goodnightServers.prototype.shouldToggle = this.parseBoolean($("#shouldToggleInput").val(), "Toggle", "Held");

    this.saveSettings();
}

goodnightServers.prototype.keyDown = function(e)
{
    // If toggling is enabled, let keyUp handle the event
    if (this.shouldToggle)
        return;

    // Grab the keyCode of the key that's down
    var kc = e.which || e.keyCode;

    // If it's the key that's set in the settings, show the server list
    if (kc === this.keyCode)
        this.show();
}

goodnightServers.prototype.keyUp = function(e)
{
    // Grab the keyCode of the key that's up
    var kc = e.which || e.keyCode;

    // If it's the key that's set in the settings
    if (kc === this.keyCode)
    {
        // If toggling is disabled, hide the server list
        if (!this.shouldToggle)
            this.hide();
        // If toggling is enabled, toggle the server list
        else
        {
            if (this.tuckedIn)
                this.show();
            else
                this.hide();
        }
    }
}

goodnightServers.prototype.getSettingsPanel = function()
{
    // Mostly liking how this is laid out, but would be interested in finding a library
    // that uses the factory pattern to create DOM elements with different style's and such

    // Not a fan of having to do modify the HTML this way, attempted to do it in a script tag, but the script wasn't executing
    var optionHeldHTML = '<option value="Held">Held</option>';
    var optionToggleHTML = '<option value="Toggle">Toggle</option>';

    if (!this.shouldToggle)
        optionHeldHTML = optionHeldHTML.insert(optionHeldHTML.indexOf('>'), ' selected');
    else
        optionToggleHTML = optionToggleHTML.insert(optionToggleHTML.indexOf('>'), ' selected');

    var settingsPanelHTML =
    '<div style="border: 2px solid; border-color: hsla(218, 5%, 47%, .3); border-radius: 5px; padding: 5px;">' + 
        '<div style="margin-top: 0; margin-bottom: 30px; font-size: 22px; color: #fff; text-decoration: underline;">' +
            'Goodnight Servers\' Settings' +
        '</div>' +
        '<div style="margin: 0">' +
            '<div style="width: 100%; margin: 0; margin-top: 0; margin-bottom: 10px; text-align: center;">' +
                '<span style="position: absolute; left: 60px; color: #fff;">' +
                    'KeyCode:' +
                '</span>' +
                '<input id="keyCodeInput" style="width: 150px; padding-left: 2px; padding-right: 2px;" value="' + this.keyCode + '" />' + 
            '</div>' +
            '<div style="width: 100%; margin: 0; margin-top: 10px; margin-bottom: 0; text-align: center;">' +
                '<span style="position: absolute; left: 60px; color: #fff;">' +
                    'Transition Time:' +
                '</span>' +
                '<input id="transitionTimeInput" style="width: 150px; padding-left: 2px; padding-right: 2px;" value="' + this.transitionTime + '" />' + 
            '</div>' +
            '<div style="width: 100%; margin: 0; margin-top: 10px; margin-bottom: 0; text-align: center;">' +
                '<span style="position: absolute; left: 60px; color: #fff;">' +
                    'Should Toggle:' +
                '</span>' +
                '<select id="shouldToggleInput" style="width: 150px; padding-top: 2px; padding-bottom: 2px;">' +
                    optionHeldHTML +
                    optionToggleHTML +
                '</select>' +
            '</div>' +
        '</div>' +
        '<div style="text-align: center; margin-top: 25px; margin-bottom: 25px;">' +
            '<button style="width: 100px; line-height: 26px;" onClick="goodnightServers.prototype.applySettings()">Apply</button>' +
        '</div>' +
        '<div style="width: 100%; text-align: center; margin-top: 0;">' +
            '<a href="https://bitbucket.org/GeoDoX/">GeoDoX on BitBucket</a>' +
        '</div>' +
    '</div>';

    return settingsPanelHTML;
}

goodnightServers.prototype.getName = function()
{
    return this.name;
}

goodnightServers.prototype.getDescription = function()
{
    return 'Tucks the server list into bed until the TAB (default) is held. Can be changed to toggle in settings.\n' +
            'Thanks to HansAnonymous for helping.';
}

goodnightServers.prototype.getVersion = function()
{
    return "1.0.0";
}

goodnightServers.prototype.getAuthor = function()
{
    return 'GeoDoX';
}

// Unused Functions
goodnightServers.prototype.load = function() {}         // Not really ever used and will be removed in v2
goodnightServers.prototype.unload = function() {}       // Not really ever used and will be removed in v2
goodnightServers.prototype.onSwitch = function() {}     // Not entirely sure what this does, but it's not necessary for this plugin
goodnightServers.prototype.onMessage = function() {}    // Not necessary for this plugin
goodnightServers.prototype.observer = function() {}     // I believe this is ES7's observer pattern, but I'm not sure. Regardless, not necessary for this plugin

// Helpful functions
goodnightServers.prototype.namespaceEvent = function(e, namespace)
{
    // Probably not needed, but it helps to explain what [event].[namespace] is
    return e + "." + namespace;
}

goodnightServers.prototype.parseBoolean = function(value, trueValue = 'true', falseValue = 'false', ignoreCase = true)
{
    // Parses a value that will be checked against two values representing true and false,
    // returning true if the value === trueValue, or returning false if the value === the false value
    // Throws an Error if the value didn't === the true or the false value
    // Optionally ignores case, enabled by default

    // Convert the value, trueValue, and falseValue to lower case if each are instances of strings and the case should be ignored
    if (value instanceof String && ignoreCase)
        value = value.toLowerCase();
    if (trueValue instanceof String && ignoreCase)
        trueValue = trueValue.toLowerCase();
    if (falseValue instanceof String && ignoreCase)
        falseValue = falseValue.toLowerCase();

    // Check value against trueValue
    if (value === trueValue)
        return true;
    // Check value against falseValue
    else if (value === falseValue)
        return false;
    // Throw an Error because it was neither true nor false
    else
        throw new Error("Value was neither the equivalent of the true value or the false value.");
}

String.prototype.insert = String.prototype.insert || function (index, string)
{
    if (index > 0)
        return this.substring(0, index) + string + this.substring(index, this.length);
    else
        return string + this;
}